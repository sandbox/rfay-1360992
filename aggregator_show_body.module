<?php
// $Id$
/**
 * @file aggregator_show_body.module
 * Modify Aggregator module's blocks so that they can show the body instead of a
 * link.
 *
 * The idea here is to do nothing but show the *content* of the most recent
 * RSS item.
 *
 * To use this you have to set up a feed in Aggregator module.
 *
 * This is a stopgap until we have Feeds in Drupal 7.
 */


/**
 * Implements hook_block_info().
 */
function aggregator_show_body_block_info() {
  $block = array();
  $result = db_query('SELECT cid, title FROM {aggregator_category} ORDER BY title');
  foreach ($result as $category) {
    $block['aggregator_show_body_category-' . $category->cid]['info'] = t('Aggregator Show Body: !title category latest items', array('!title' => $category->title));
  }
  $result = db_query('SELECT fid, title FROM {aggregator_feed} WHERE block <> 0 ORDER BY fid');
  foreach ($result as $feed) {
    $block['aggregator_show_body_feed-' . $feed->fid]['info'] = t('Aggregator Show Body: !title feed latest items', array('!title' => $feed->title));
  }
  return $block;
}


/**
 * Implements hook_block_view().
 *
 * Generates blocks for the latest news items in each category and feed.
 */
function aggregator_show_body_block_view($delta = '') {
  if (user_access('access news feeds')) {
    $block = array();
    list($type, $id) = explode('-', $delta);
    switch ($type) {
      case 'aggregator_show_body_feed':
        if ($feed = db_query('SELECT fid, title, block FROM {aggregator_feed} WHERE block <> 0 AND fid = :fid', array(':fid' => $id))->fetchObject()) {
          $block['subject'] = check_plain($feed->title);
          $result = db_query_range("SELECT * FROM {aggregator_item} WHERE fid = :fid ORDER BY timestamp DESC, iid DESC", 0, $feed->block, array(':fid' => $id));
        }
        break;

      case 'aggregator_show_body_category':
        if ($category = db_query('SELECT cid, title, block FROM {aggregator_category} WHERE cid = :cid', array(':cid' => $id))->fetchObject()) {
          $block['subject'] = check_plain($category->title);
          $result = db_query_range('SELECT i.* FROM {aggregator_category_item} ci LEFT JOIN {aggregator_item} i ON ci.iid = i.iid WHERE ci.cid = :cid ORDER BY i.timestamp DESC, i.iid DESC', 0, $category->block, array(':cid' => $category->cid));
        }
        break;
    }
    if (!empty($result)) {
      foreach ($result as $item) {
        $block['subject'] .= ' ' . format_date($item->timestamp, 'medium', '', 'America/Denver');
        $block['content'][]['#markup'] = theme('aggregator_show_body_block_item', array('item' => $item));
        break;
      }
    }
    return $block;
  }
}

/**
 * Implements hook_theme().
 */
function aggregator_show_body_theme() {
  return array(
    'aggregator_show_body_block_item' => array(
      'render element' => 'item',
    ),
  );
}

/**
 * Returns HTML for an individual feed item for display in the block.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: The item to be displayed.
 *   - feed: Not used.
 *
 * @ingroup themeable
 */
function theme_aggregator_show_body_block_item($variables) {
  $markup .= '<div class="aggregator-item-description">' . check_markup($variables['item']->description, 1) . '</div>';
  return $markup;
}


/**
 * Implements hook_aggregator_process_info().
 */
function aggregator_show_body_aggregator_process_info() {
  return array(
    'title' => t('Save items even if previously seen'),
    'description' => t('Same as standard processor, but it does not ignore changed feed items.'),
  );
}

/**
 * Implements hook_aggregator_process().
 *
 * This feed processor is the same as the standard one, but it uses
 * a different function to save the item (and that different function
 * saves the item even if it has been seen before.
 */
function aggregator_show_body_aggregator_process($feed) {
  if (is_object($feed)) {
    if (is_array($feed->items)) {
      foreach ($feed->items as $item) {
        // Save this item. Try to avoid duplicate entries as much as possible. If
        // we find a duplicate entry, we resolve it and pass along its ID is such
        // that we can update it if needed.
        if (!empty($item['guid'])) {
          $entry = db_query("SELECT iid, timestamp FROM {aggregator_item} WHERE fid = :fid AND guid = :guid", array(':fid' => $feed->fid, ':guid' => $item['guid']))->fetchObject();
        }
        elseif ($item['link'] && $item['link'] != $feed->link && $item['link'] != $feed->url) {
          $entry = db_query("SELECT iid, timestamp FROM {aggregator_item} WHERE fid = :fid AND link = :link", array(':fid' => $feed->fid, ':link' => $item['link']))->fetchObject();
        }
        else {
          $entry = db_query("SELECT iid, timestamp FROM {aggregator_item} WHERE fid = :fid AND title = :title", array(':fid' => $feed->fid, ':title' => $item['title']))->fetchObject();
        }
        if (!$item['timestamp']) {
          $item['timestamp'] = isset($entry->timestamp) ? $entry->timestamp : REQUEST_TIME;
        }
        aggregator_show_body_save_item(array('iid' => (isset($entry->iid) ? $entry->iid : ''), 'fid' => $feed->fid, 'timestamp' => $item['timestamp'], 'title' => $item['title'], 'link' => $item['link'], 'author' => $item['author'], 'description' => $item['description'], 'guid' => $item['guid']));
      }
    }
  }
}

/**
 * Add/edit/delete an aggregator item.
 *
 * This is different from aggregator_save_item() in that it updates a feed
 * item that it has already seen instead of skipping over it.
 *
 * That is probably somewhat less efficient, but also gives us real data
 * in the feed instead of stale data.
 *
 * @param $edit
 *   An associative array describing the item to be added/edited/deleted.
 */
function aggregator_show_body_save_item($edit) {
  // If we already have the item (if iid is set) then update it.
  if ($edit['title'] && !empty($edit['iid'])) {
    db_update('aggregator_item')
      ->fields(array(
        'title' => $edit['title'],
        'link' => $edit['link'],
        'author' => $edit['author'],
        'description' => $edit['description'],
        'guid' => $edit['guid'],
        'timestamp' => $edit['timestamp'],
        'fid' => $edit['fid'],
      ))
      ->condition(
        'iid', $edit['iid']
      )
      ->execute();
  }
  // Otherwise, if the item has not been saved, insert it.
  else if ($edit['title'] && empty($edit['iid'])) {
    $edit['iid'] = db_insert('aggregator_item')
      ->fields(array(
        'title' => $edit['title'],
        'link' => $edit['link'],
        'author' => $edit['author'],
        'description' => $edit['description'],
        'guid' => $edit['guid'],
        'timestamp' => $edit['timestamp'],
        'fid' => $edit['fid'],
      ))
      ->execute();
  }
  if ($edit['iid'] && !$edit['title']) {
    db_delete('aggregator_item')
      ->condition('iid', $edit['iid'])
      ->execute();
    db_delete('aggregator_category_item')
      ->condition('iid', $edit['iid'])
      ->execute();
  }
  elseif ($edit['title'] && $edit['link']) {
    // file the items in the categories indicated by the feed
    $result = db_query('SELECT cid FROM {aggregator_category_feed} WHERE fid = :fid', array(':fid' => $edit['fid']));
    foreach ($result as $category) {
      db_merge('aggregator_category_item')
        ->key(array('iid' => $edit['iid']))
        ->fields(array(
          'cid' => $category->cid,
        ))
        ->execute();
    }
  }
}